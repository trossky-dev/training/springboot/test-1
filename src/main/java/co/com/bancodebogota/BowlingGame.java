package co.com.bancodebogota;

public class BowlingGame {
	private int[] rolls = new int[21];
	private  int currentRoll=0;


	public void roll(int poins) {
		rolls[currentRoll++]= poins;
	}

	public int score() {
		int score = 0;
		int frameIndex =0;
		for (int frame = 0; frame < 10; frame++) {

			if(isStrike(rolls[frameIndex])){
				score += 10 + strikeBouns(frameIndex);
				frameIndex++;

			}
			else if (isSpare(frameIndex)){// spare
				score +=10+ spareBouns(frameIndex, 2);
				frameIndex += 2;
			}else{
				score += sumOfBallsInFrame(frameIndex);
				frameIndex += 2;
			}
		}
		return score;
	}

	private int sumOfBallsInFrame(int frameIndex) {
		return rolls[frameIndex]+ rolls[frameIndex + 1];
	}

	private int spareBouns(int frameIndex, int i) {
		return rolls[frameIndex + i];
	}

	private int strikeBouns(int frameIndex) {
		return rolls[frameIndex + 1] + rolls[frameIndex + 2];
	}

	private boolean isStrike(int roll) {
		return roll ==10;
	}

	private boolean isSpare(int frameIndex) {
		return rolls[frameIndex]+ rolls[frameIndex+1]==10;
	}
}
